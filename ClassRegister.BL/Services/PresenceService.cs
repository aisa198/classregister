﻿using ClassRegister.BL.BusinessModels;
using ClassRegister.BL.Mapping;
using ClassRegister.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ClassRegister.BL.Services
{
    public class PresenceService
    {
        private PresenceRepository _repository;

        public PresenceService()
        {
            _repository = new PresenceRepository();
        }

        public void AddPresence(PresenceBl presenceBl)
        {
            var presence = PresenceMapper.MapPresenceBlToPresence(presenceBl);
            presence.Student = StudentMapper.MapStudentBlToStudent(presenceBl.StudentBl);
            presence.Register = RegisterMapper.MapRegisterBlToRegister(presenceBl.RegisterBl);
            _repository.AddPresence(presence);
        }

        public PresenceResultBl CountResultOfPresence(List<PresenceBl> presencesBl, int studentId, int daysOfCourse, int requiredPercentForPresence)
        {
            var presenceResultBl = new PresenceResultBl
            {
                PresenceScore = presencesBl
                .Where(x => x.StudentBl.Id == studentId)
                .Sum(x => x.PresenceOfStudent)
            };
            presenceResultBl.PercentOfPresence = Math.Round(presenceResultBl.PresenceScore * 100.0 / daysOfCourse);
            presenceResultBl.ResultOfPresence = (presenceResultBl.PercentOfPresence >= requiredPercentForPresence) ? "Zaliczone" : "Niezaliczone";
            return presenceResultBl;
        }
    }
}

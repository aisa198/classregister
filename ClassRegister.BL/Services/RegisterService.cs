﻿using ClassRegister.BL.BusinessModels;
using ClassRegister.BL.Mapping;
using ClassRegister.DAL.Models;
using ClassRegister.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ClassRegister.BL.Services
{
    public class RegisterService
    {
        private RegisterRepository _repository;

        public RegisterService()
        {
            _repository = new RegisterRepository();
        }

        public void AddRegister(RegisterBl registerBl)
        {
            var register = RegisterMapper.MapRegisterBlToRegister(registerBl);
            register.ListOfStudents = registerBl.ListOfStudentsBl?
                .Select(x => StudentMapper
                .MapStudentBlToStudent(x))
                .ToList();
            _repository.AddRegister(register);
        }

        public List<RegisterBl> GetAllRegisters()
        {
            return _repository.GetAll().Select(x => RegisterMapper.MapRegisterToRegisterBl(x)).ToList();
        }

        public RegisterBl GetRegister(int id)
        {
            var register = _repository.GetRegisterWithStudents(id);
            var registerBl = RegisterMapper.MapRegisterToRegisterBl(register);
            registerBl.ListOfStudentsBl = register.ListOfStudents?
                .Select(x => StudentMapper
                .MapStudentToStudentBl(x))
                .ToList();
            return registerBl;
        }

        public CourseReportBl CreateReport(int id)
        {
            var register = _repository.GetRegisterWithStudentsHomeworksAndPresences(id);
            var registerBl = RegisterMapper.MapRegisterToRegisterBl(register);
            registerBl.ListOfStudentsBl = register.ListOfStudents?
                .Select(x => StudentMapper
                .MapStudentToStudentBl(x))
                .ToList();
            registerBl.HomeworksBl = register.Homeworks?
                .Select(x => HomeworkMapper
                .MapHomeworkToHomeworkBl(x))
                .ToList();
            registerBl.PresencesBl = register.Presences?
                .Select(x => PresenceMapper
                .MapPresenceToPresenceBl(x))
                .ToList();
            var courseReportBl = new CourseReportBl
            {
                NameOfCourse = registerBl.NameOfCourse,
                DateOfStart = registerBl.DateOfStart,
                RequiredPercentForHomework = registerBl.RequiredPercentForHomework,
                RequiredPercentForPresence = registerBl.RequiredPercentForPresence,
                NumberOfStudents = registerBl.NumberOfStudents,
                MaxScore = registerBl.HomeworksBl.Sum(x => x.MaxScore) / registerBl.NumberOfStudents,
                DaysOfCourse = registerBl.PresencesBl.Count / register.NumberOfStudents,
                ListOfStudentsBl = registerBl.ListOfStudentsBl,
                PresencesBl = registerBl.PresencesBl,
                HomeworksBl = registerBl.HomeworksBl
            };
            return courseReportBl;
        }
    }
}

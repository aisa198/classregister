﻿using ClassRegister.BL.BusinessModels;
using ClassRegister.BL.Mapping;
using ClassRegister.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ClassRegister.BL.Services
{
    public class HomeworkService
    {
        private HomeworkRepository _repository;

        public HomeworkService()
        {
            _repository = new HomeworkRepository();
        }

        public void AddHomework(HomeworkBl homeworkBl)
        {
            var homework = HomeworkMapper.MapHomeworkBlToHomework(homeworkBl);
            homework.Student = StudentMapper.MapStudentBlToStudent(homeworkBl.StudentBl);
            homework.Register = RegisterMapper.MapRegisterBlToRegister(homeworkBl.RegisterBl);
            _repository.AddHomework(homework);
        }

        public HomeworkResultBl CountResultOfHomework(List<HomeworkBl> homeworksBl, int studentId, int maxScore, int requiredPercentForHomework)
        {
            var homeworkResultBl = new HomeworkResultBl
            {
                HomeworkScore = homeworksBl
                .Where(x => x.StudentBl.Id == studentId)
                .Sum(x => x.Score)
            };
            homeworkResultBl.PercentOfHomework = Math.Round(homeworkResultBl.HomeworkScore * 100.0 / maxScore);
            homeworkResultBl.ResultOfHomework = (homeworkResultBl.PercentOfHomework >= requiredPercentForHomework) ? "Zaliczone" : "Niezaliczone";
            return homeworkResultBl;
        }
    }
}

﻿using ClassRegister.BL.BusinessModels;
using ClassRegister.BL.Mapping;
using ClassRegister.DAL.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace ClassRegister.BL.Services
{
    public class StudentService
    {
        private StudentRepository _repository;
        public StudentService()
        {
            _repository = new StudentRepository();
        }

        public void AddStudent(StudentBl studentBl)
        {
            var student = StudentMapper.MapStudentBlToStudent(studentBl);
            _repository.AddStudent(student);
        }

        public List<StudentBl> GetAllStudents()
        {
            return _repository.GetAll().Select(x => StudentMapper.MapStudentToStudentBl(x)).ToList();
        }

        public StudentBl GetStudent(int id)
        {
            var student = _repository.GetStudent(id);
            var studentBl = StudentMapper.MapStudentToStudentBl(student);
            return studentBl;
        }
    }
}

﻿namespace ClassRegister.BL.BusinessModels
{
    public class HomeworkResultBl
    {
        public int HomeworkScore { get; set; }
        public double PercentOfHomework { get; set; }
        public string ResultOfHomework { get; set; }
    }
}

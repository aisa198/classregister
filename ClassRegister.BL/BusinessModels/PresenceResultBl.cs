﻿namespace ClassRegister.BL.BusinessModels
{
    public class PresenceResultBl
    {
        public int PresenceScore { get; set; }
        public double PercentOfPresence { get; set; }
        public string ResultOfPresence { get; set; }
    }
}

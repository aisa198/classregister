﻿using ClassRegister.DAL.Models;
using System.Collections.Generic;

namespace ClassRegister.BL.BusinessModels
{
    public class HomeworkBl
    {
        public int Id { get; set; }
        public int MaxScore { get; set; }
        public int Score { get; set; }
        public RegisterBl RegisterBl { get; set; }
        public StudentBl StudentBl { get; set; }
    }
}

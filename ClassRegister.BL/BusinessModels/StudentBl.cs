﻿using ClassRegister.DAL.Models;
using System;
using System.Collections.Generic;

namespace ClassRegister.BL.BusinessModels
{
    public class StudentBl
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public long Pesel { get; set; }
        public DateTime DateOfBirth { get; set; }
        public char Gender { get; set; }
        public List<RegisterBl> RegistersBl { get; set; }
        public List<HomeworkBl> HomeworksBl { get; set; }
        public List<PresenceBl> PresencesBl { get; set; }
    }
}

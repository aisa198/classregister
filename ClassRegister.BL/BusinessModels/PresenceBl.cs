﻿using System;

namespace ClassRegister.BL.BusinessModels
{
    public class PresenceBl
    {
        public int Id { get; set; }
        public DateTime DaysOfCourse { get; set; }
        public int PresenceOfStudent { get; set; }
        public RegisterBl RegisterBl { get; set; }
        public StudentBl StudentBl { get; set; }
    }
}

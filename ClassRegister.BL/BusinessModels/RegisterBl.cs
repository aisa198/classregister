﻿using ClassRegister.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassRegister.BL.BusinessModels
{
    public class RegisterBl
    {
        public int Id { get; set; }
        public string NameOfCourse { get; set; }
        public string NameOfTeacher { get; set; }
        public DateTime DateOfStart { get; set; }
        public int RequiredPercentForHomework { get; set; }
        public int RequiredPercentForPresence { get; set; }
        public int NumberOfStudents { get; set; }
        public List<StudentBl> ListOfStudentsBl { get; set; }
        public List<HomeworkBl> HomeworksBl { get; set; }
        public List<PresenceBl> PresencesBl { get; set; }
    }
}

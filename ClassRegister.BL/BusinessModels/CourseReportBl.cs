﻿using System;
using System.Collections.Generic;

namespace ClassRegister.BL.BusinessModels
{
    public class CourseReportBl
    {
        public string NameOfCourse { get; set; }
        public DateTime DateOfStart { get; set; }
        public int RequiredPercentForHomework { get; set; }
        public int RequiredPercentForPresence { get; set; }
        public int NumberOfStudents { get; set; }
        public int MaxScore { get; set; }
        public int DaysOfCourse { get; set; }
        public List<StudentBl> ListOfStudentsBl { get; set; }
        public List<HomeworkBl> HomeworksBl { get; set; }
        public List<PresenceBl> PresencesBl { get; set; }
    }
}

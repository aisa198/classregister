﻿using ClassRegister.BL.BusinessModels;
using ClassRegister.DAL.Models;

namespace ClassRegister.BL.Mapping
{
    public class HomeworkMapper
    {
        public static Homework MapHomeworkBlToHomework(HomeworkBl homeworkBl)
        {
            if (homeworkBl == null)
            {
                return null;
            }
            else
            {
                var homework = new Homework
                {
                    Id = homeworkBl.Id,
                    MaxScore = homeworkBl.MaxScore,
                    Score = homeworkBl.Score
                };

                return homework;
            }
        }

        public static HomeworkBl MapHomeworkToHomeworkBl(Homework homework)
        {
            if (homework == null)
            {
                return null;
            }
            else
            {
                var homeworkBl = new HomeworkBl
                {
                    Id = homework.Id,
                    MaxScore = homework.MaxScore,
                    Score = homework.Score,
                    StudentBl = StudentMapper.MapStudentToStudentBl(homework.Student)
                };

                return homeworkBl;
            }
        }
    }
}

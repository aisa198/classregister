﻿using ClassRegister.BL.BusinessModels;
using ClassRegister.DAL.Models;
using System.Collections.Generic;

namespace ClassRegister.BL.Mapping
{
    public class RegisterMapper
    {
        public static Register MapRegisterBlToRegister(RegisterBl registerBl)
        {
            if (registerBl == null)
            {
                return null;
            }
            else
            {
                var register = new Register
                {
                    Id = registerBl.Id,
                    NameOfCourse = registerBl.NameOfCourse,
                    NameOfTeacher = registerBl.NameOfTeacher,
                    DateOfStart = registerBl.DateOfStart,
                    RequiredPercentForHomework = registerBl.RequiredPercentForHomework,
                    RequiredPercentForPresence = registerBl.RequiredPercentForPresence,
                    NumberOfStudents = registerBl.NumberOfStudents
                };

                return register;
            }
        }

        public static RegisterBl MapRegisterToRegisterBl(Register register)
        {
            if (register == null)
            {
                return null;
            }
            else
            {
                var registerBl = new RegisterBl
                {
                    Id = register.Id,
                    NameOfCourse = register.NameOfCourse,
                    NameOfTeacher = register.NameOfTeacher,
                    DateOfStart = register.DateOfStart,
                    RequiredPercentForHomework = register.RequiredPercentForHomework,
                    RequiredPercentForPresence = register.RequiredPercentForPresence,
                    NumberOfStudents = register.NumberOfStudents
                };

                return registerBl;
            }
        }
    }
}

﻿using ClassRegister.BL.BusinessModels;
using ClassRegister.DAL.Models;

namespace ClassRegister.BL.Mapping
{
    public class PresenceMapper
    {
        public static Presence MapPresenceBlToPresence(PresenceBl presenceBl)
        {
            if (presenceBl == null)
            {
                return null;
            }
            else
            {
                var presence = new Presence
                {
                    Id = presenceBl.Id,
                    DaysOfCourse = presenceBl.DaysOfCourse,
                    PresenceOfStudent = presenceBl.PresenceOfStudent
                };

                return presence;
            }
        }

        public static PresenceBl MapPresenceToPresenceBl(Presence presence)
        {
            if (presence == null)
            {
                return null;
            }
            else
            {
                var presenceBl = new PresenceBl
                {
                    Id = presence.Id,
                    DaysOfCourse = presence.DaysOfCourse,
                    PresenceOfStudent = presence.PresenceOfStudent,
                    StudentBl = StudentMapper.MapStudentToStudentBl(presence.Student)
                };

                return presenceBl;
            }
        }
    }
}

﻿using ClassRegister.BL.BusinessModels;
using ClassRegister.DAL.Models;
using System.Collections.Generic;

namespace ClassRegister.BL.Mapping
{
    public class StudentMapper
    {
        public static Student MapStudentBlToStudent(StudentBl studentBl)
        {
            if (studentBl == null)
            {
                return null;
            }
            else
            {
                var student = new Student
                {
                    Id = studentBl.Id,
                    Name = studentBl.Name,
                    Pesel = studentBl.Pesel,
                    DateOfBirth = studentBl.DateOfBirth,
                    Gender = studentBl.Gender
                };

                return student;
            }
        }

        public static StudentBl MapStudentToStudentBl(Student student)
        {
            if (student == null)
            {
                return null;
            }
            else
            {
                var studentBl = new StudentBl
                {
                    Id = student.Id,
                    Name = student.Name,
                    Pesel = student.Pesel,
                    DateOfBirth = student.DateOfBirth,
                    Gender = student.Gender
                };

                return studentBl;
            }
        }
    }
}

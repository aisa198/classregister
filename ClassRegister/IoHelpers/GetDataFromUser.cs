﻿using System;
using System.Globalization;

namespace ClassRegister.CLI.IoHelpers
{
    public class GetDataFromUser
    {
        public void WriteMessage(string message)
        {
            Console.Write(message);
        }

        public void WriteMessageLine(string message)
        {
            Console.WriteLine(message);
        }

        public string GetData(string messageToUser)
        {
            WriteMessage(messageToUser);
            string input = Console.ReadLine().Trim();
            while (input == "")
            {
                input = GetData("Podaj dane: ");
            }
            return input;
        }

        public string GetName(string messageToUser)
        {
            string name = GetData(messageToUser);
            while (name.Length < 2)
            {
                name = GetData("Zbyt mało znaków. Podaj właściwe dane:");
            }
            string[] parts = name.Split(' ');
            while (parts.Length != 2)
            {
                name = GetData("Zła ilość słów. Podaj właściwe dane:");
                parts = name.Split(' ');
            }
            return name;
        }

        public DateTime GetDate(string messageToUser)
        {
            string date = GetData("(MM-DD-RRRR) " + messageToUser);
            DateTime validDate;
            DateTime.TryParse("01-01-1753", out var dateToCheck);
            while (!(DateTime.TryParse(date, CultureInfo.InvariantCulture, DateTimeStyles.None, out validDate)) || validDate < dateToCheck)
            {
                date = GetData("Podaj prawidłową datę (MM-DD-RRRR): ");
            }
            return validDate;
        }

        public DateTime GetDateOfBirth(string messageToUser)
        {
            string date = GetData("(MM-DD-RRRR) " + messageToUser);
            DateTime validDate;
            DateTime.TryParse("01-01-1753", out var dateToCheck);
            while (!(DateTime.TryParse(date, CultureInfo.InvariantCulture, DateTimeStyles.None, out validDate)) || validDate > DateTime.Now || validDate < dateToCheck)
            {
                date = GetData("Podaj prawidłową datę urodzenia (MM-DD-RRRR): ");
            }
            return validDate;
        }

        public int GetNumber(string messageToUser, int min, int max)
        {
            string input = GetData(messageToUser);
            int result;
            while (!int.TryParse(input, out result) || result < min || result > max)
            {
                input = GetData($"Podaj prawidłowe dane (liczbę z zakresu {min}-{max}): ");
            }
            return result;
        }

        public long GetPesel(string messageToUser)
        {
            string input = GetData(messageToUser);
            long result;
            while (!long.TryParse(input, out result) || result.ToString().Length != 11)
            {
                input = GetData("Podaj prawidłowe dane (11 cyfr): ");
            }
            return result;
        }

        public char GetGender(string messageToUser)
        {
            string input = GetData(messageToUser);
            char result;
            while (!char.TryParse(input, out result) || (result != 'K' && result != 'M'))
            {
                input = GetData("Podaj prawidłowe dane (K/M): ");
            }
            return result;
        }
    }
}

﻿using ClassRegister.BL.Services;
using ClassRegister.BL.BusinessModels;
using System.Collections.Generic;
using System.Linq;
using System;

namespace ClassRegister.CLI.IoHelpers
{
    public class MethodsFromMenu
    {
        private GetDataFromUser _data;
        private RegisterService _registerService;
        private StudentService _studentService;
        private HomeworkService _homeworkService;
        private PresenceService _presenceService;

        public MethodsFromMenu()
        {
            _data = new GetDataFromUser();
            _registerService = new RegisterService();
            _studentService = new StudentService();
            _homeworkService = new HomeworkService();
            _presenceService = new PresenceService();
        }

        public void AddStudent()
        {
            var pesel = _data.GetPesel("Podaj PESEL studenta: ");
            var listOfStudents = _studentService.GetAllStudents();
            while (listOfStudents.Any(x => x.Pesel == pesel))
            {
                _data.WriteMessageLine("Student o podanym numerze PESEL już istnieje.");
                pesel = _data.GetPesel("Podaj PESEL studenta: ");
            }
            var studentBl = new StudentBl
            {
                Pesel = pesel,
                Name = _data.GetName("Podaj imię i nazwisko studenta: "),
                DateOfBirth = _data.GetDateOfBirth("Podaj datę urodzenia: "),
                Gender = _data.GetGender("Podaj płeć (K/M): ")
            };
            _studentService.AddStudent(studentBl);
        }

        public void AddRegister()
        {
            var listOfStudents = _studentService.GetAllStudents();

            if (listOfStudents.Count != 0)
            {
                var registerBl = new RegisterBl
                {
                    NameOfCourse = _data.GetData("Podaj nazwę kursu: "),
                    NameOfTeacher = _data.GetName("Podaj imię i nazwisko prowadzącego: "),
                    DateOfStart = _data.GetDate("Podaj datę rozpoczęcia kursu: "),
                    RequiredPercentForHomework = _data.GetNumber("Podaj próg procentowy dla prac domowych: ", 1, 100),
                    RequiredPercentForPresence = _data.GetNumber("Podaj próg procentowy dla obecności: ", 1, 100),
                    NumberOfStudents = _data.GetNumber("Podaj liczbę kursantów: ", 1, 20)
                };
                if (listOfStudents.Count >= registerBl.NumberOfStudents)
                {
                    registerBl.ListOfStudentsBl = new List<StudentBl>();
                    _data.WriteMessageLine("Oto dostępni kursanci: ");
                    listOfStudents.ForEach(x => _data.WriteMessageLine($"{x.Id}. {x.Name}"));

                    for (var i = 1; i <= registerBl.NumberOfStudents; i++)
                    {
                        var id = _data.GetNumber($"Podaj ID studenta nr {i}: ", 1, listOfStudents.Count);
                        var addedStudent = _studentService.GetStudent(id);
                        if (addedStudent != null && !registerBl.ListOfStudentsBl.Any(x => x.Id == id))
                        {
                            registerBl.ListOfStudentsBl.Add(addedStudent);
                        }
                        else
                        {
                            _data.WriteMessageLine("Nie można dodać studenta o podanym ID (ID nie istnieje lub student już został dodany)");
                            i--;
                        }
                    }
                    _registerService.AddRegister(registerBl);
                }
                else
                {
                    _data.WriteMessageLine("Zbyt mało studentów w bazie, by utworzyc podany kurs.");
                }
            }
            else
            {
                _data.WriteMessageLine("Nie ma dostępnych studentów. Najpierw dodaj studentów.");
            }
        }

        public int ChooseActiveRegister()
        {
            var courses = _registerService.GetAllRegisters();
            if (courses.Count != 0)
            {
                _data.WriteMessageLine("Oto dostępne kursy:");
                courses.ForEach(x => _data.WriteMessageLine($"{x.Id}. {x.NameOfCourse}"));

                return (_data.GetNumber("Podaj ID kursu, którym chcesz zarządzać: ", 1, courses.Count));
            }
            else
            {
                _data.WriteMessageLine("Nie ma dostępnych kursów. Najpierw stwórz kurs.");
                return 0;
            }
        }

        public void AddPresence(int id)
        {
            var dateOfPresence = _data.GetDate("Podaj dzień: ");
            _data.WriteMessageLine("Dla każdego z uczniów podaj obecność (1 - obecny, 0 - nieobecny)");
            var activeCourseBl = _registerService.GetRegister(id);
            foreach (var studentBl in activeCourseBl.ListOfStudentsBl)
            {
                var presenceBl = new PresenceBl()
                {
                    DaysOfCourse = dateOfPresence,
                    PresenceOfStudent = _data.GetNumber($"{studentBl.Name}: ", 0, 1),
                    RegisterBl = activeCourseBl,
                    StudentBl = studentBl
                };
                _presenceService.AddPresence(presenceBl);
            }
        }

        public void AddHomework(int id)
        {
            var maxScore = _data.GetNumber("Podaj maksymalna ilość punktów do zdobycia: ", 1, 300);
            _data.WriteMessageLine("Dla każdego z uczniów podaj ilość zdobytych punktów");
            var activeCourseBl = _registerService.GetRegister(id);
            foreach (var studentBl in activeCourseBl.ListOfStudentsBl)
            {
                var homeworkBl = new HomeworkBl()
                {
                    MaxScore = maxScore,
                    Score = _data.GetNumber($"{studentBl.Name}: ", 0, maxScore),
                    RegisterBl = activeCourseBl,
                    StudentBl = studentBl
                };
                _homeworkService.AddHomework(homeworkBl);
            }
        }

        public void CreateReport(int id)
        {
            var courseReportBl = _registerService.CreateReport(id);
            if (courseReportBl != null)
            {
                _data.WriteMessageLine("***RAPORT Z KURSU***");
                _data.WriteMessageLine($"Nazwa kursu: {courseReportBl.NameOfCourse}");
                _data.WriteMessageLine($"Data rozpoczęcia kursu: {courseReportBl.DateOfStart.ToShortDateString()}");
                _data.WriteMessageLine($"Próg z obecności: {courseReportBl.RequiredPercentForPresence}%");
                var counter = 1;
                foreach (var student in courseReportBl.ListOfStudentsBl)
                {
                    var resultOfPresence = _presenceService.CountResultOfPresence(courseReportBl.PresencesBl, student.Id, courseReportBl.DaysOfCourse, courseReportBl.RequiredPercentForPresence);
                    _data.WriteMessageLine($"\t {counter}. {student.Name} {resultOfPresence.PresenceScore}/{courseReportBl.DaysOfCourse} ({resultOfPresence.PercentOfPresence}%) - {resultOfPresence.ResultOfPresence}");
                    counter++;
                }
                _data.WriteMessageLine($"Próg z pracy domowej: {courseReportBl.RequiredPercentForHomework}%");
                foreach (var student in courseReportBl.ListOfStudentsBl)
                {
                    var resultOfHomework = _homeworkService.CountResultOfHomework(courseReportBl.HomeworksBl, student.Id, courseReportBl.MaxScore, courseReportBl.RequiredPercentForHomework);
                    _data.WriteMessageLine($"\t {counter}. {student.Name} {resultOfHomework.HomeworkScore}/{courseReportBl.MaxScore} ({resultOfHomework.PercentOfHomework}%) - {resultOfHomework.ResultOfHomework}");
                    counter++;
                }
            }
            else
            {
                _data.WriteMessageLine("Brak kursu o podanym ID w bazie danych.");
            }
        }
    }
}

﻿using ClassRegister.BL.Services;
using ClassRegister.CLI.IoHelpers;

namespace ClassRegister.CLI
{
    class ProgramLoop
    {
        private MethodsFromMenu _menu;
        private GetDataFromUser _data;

        public ProgramLoop()
        {
            _menu = new MethodsFromMenu();
            _data = new GetDataFromUser();
        }

        public void Run()
        {
            bool exit = false;
            do
            {
                _data.WriteMessageLine("***MENU GŁÓWNE***");
                _data.WriteMessageLine("1 - Dodaj studenta");
                _data.WriteMessageLine("2 - Dodaj kurs");
                _data.WriteMessageLine("3 - Zarządzaj kursem");
                _data.WriteMessageLine("4 - Wyjście z programu");
                switch (_data.GetNumber("Podaj liczbę (będzie ona oznaczała Twój wybór): ", 1, 4))
                {
                    case 1:
                        _menu.AddStudent();
                        break;
                    case 2:
                        _menu.AddRegister();
                        break;
                    case 3:
                        var id = _menu.ChooseActiveRegister();
                        if (id != 0)
                        {
                            RegisterMenu(id);
                        }
                        break;
                    case 4:
                        exit = true;
                        break;
                    default:
                        _data.WriteMessageLine("Nieprawidłowa komenda.");
                        break;
                }
            } while (!exit);
        }

        public void RegisterMenu(int registerId)
        {
            bool exit = false;
            do
            {
                _data.WriteMessageLine("1 - Sprawdzenie listy obecności");
                _data.WriteMessageLine("2 - Dodanie pracy domowej");
                _data.WriteMessageLine("3 - Wypisanie raportu z kursu");
                _data.WriteMessageLine("4 - Wyjście do MENU GŁÓWNEGO");
                switch (_data.GetNumber("Podaj liczbę (będzie ona oznaczała Twój wybór): ", 1, 4))
                {
                    case 1:
                        _menu.AddPresence(registerId);
                        break;
                    case 2:
                        _menu.AddHomework(registerId);
                        break;
                    case 3:
                        _menu.CreateReport(registerId);
                        break;
                    case 4:
                        exit = true;
                        break;
                    default:
                        _data.WriteMessageLine("Nieprawidłowa komenda.");
                        break;
                }
            } while (!exit);
        }
    }
}

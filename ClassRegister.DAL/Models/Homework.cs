﻿using System.Collections.Generic;

namespace ClassRegister.DAL.Models
{
    public class Homework
    {
        public int Id { get; set; }
        public int MaxScore { get; set; }
        public int Score { get; set; }
        public Register Register { get; set; }
        public Student Student { get; set; }
    }
}

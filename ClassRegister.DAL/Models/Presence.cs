﻿using System;
using System.Collections.Generic;

namespace ClassRegister.DAL.Models
{
    public class Presence
    {
        public int Id { get; set; }
        public DateTime DaysOfCourse { get; set; }
        public int PresenceOfStudent { get; set; }
        public Register Register { get; set; }
        public Student Student { get; set; }
    }
}

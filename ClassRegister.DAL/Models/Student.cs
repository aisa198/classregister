﻿using System;
using System.Collections.Generic;

namespace ClassRegister.DAL.Models
{
    public class Student
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public long Pesel { get; set; }
        public DateTime DateOfBirth { get; set; }
        public char Gender { get; set; }
        public List<Register> Registers { get; set; }
        public List<Homework> Homeworks { get; set; }
        public List<Presence> Presences { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace ClassRegister.DAL.Models
{
    public class Register
    {
        public int Id { get; set; }
        public string NameOfCourse { get; set; }
        public string NameOfTeacher { get; set; }
        public DateTime DateOfStart { get; set; }
        public int RequiredPercentForHomework { get; set; }
        public int RequiredPercentForPresence { get; set; }
        public int NumberOfStudents { get; set; }
        public List<Student> ListOfStudents { get; set; }
        public List<Homework> Homeworks { get; set; }
        public List<Presence> Presences { get; set; }
    }
}

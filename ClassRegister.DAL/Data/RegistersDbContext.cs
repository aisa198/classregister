﻿using ClassRegister.DAL.Models;
using System.Configuration;
using System.Data.Entity;

namespace ClassRegister.DAL.Data
{
    internal class RegistersDbContext : DbContext
    {
        public RegistersDbContext() : base(GetConnectionString())
        { }

        public DbSet<Student> Students { get; set; }
        public DbSet<Register> Registers { get; set; }
        public DbSet<Homework> Homeworks { get; set; }
        public DbSet<Presence> Presences { get; set; }

        private static string GetConnectionString() => ConfigurationManager.ConnectionStrings["ClassRegistersDb"].ConnectionString;
    }
}

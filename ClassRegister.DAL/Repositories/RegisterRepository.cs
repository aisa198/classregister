﻿using ClassRegister.DAL.Data;
using ClassRegister.DAL.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace ClassRegister.DAL.Repositories
{
    public class RegisterRepository
    {
        public void AddRegister(Register register)
        {
            using (var dbContext = new RegistersDbContext())
            {
                if (register.ListOfStudents != null)
                {
                    foreach (var student in register.ListOfStudents)
                    {
                        if (!dbContext.Students.Local.Contains(student))
                        {
                            dbContext.Students.Attach(student);
                            dbContext.Entry(student).State = EntityState.Modified;
                        }
                    }
                }
                dbContext.Registers.Add(register);
                dbContext.SaveChanges();
            }
        }

        public Register GetRegisterWithStudents(int id)
        {
            using (var dbContext = new RegistersDbContext())
            {
                return dbContext.Registers
                    .Include(x => x.ListOfStudents)
                    .SingleOrDefault(x => x.Id == id);
            }
        }

        public Register GetRegisterWithStudentsHomeworksAndPresences(int id)
        {
            using (var dbContext = new RegistersDbContext())
            {
                return dbContext.Registers
                    .Include(x => x.ListOfStudents)
                    .Include(x => x.Presences.Select(y => y.Student))
                    .Include(x => x.Homeworks.Select(y => y.Student))
                    .SingleOrDefault(x => x.Id == id);
            }
        }

        public List<Register> GetAll()
        {
            using (var dbContext = new RegistersDbContext())
            {
                return dbContext.Registers
                    .ToList();
            }
        }
    }
}

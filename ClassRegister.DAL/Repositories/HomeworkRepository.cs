﻿using ClassRegister.DAL.Data;
using ClassRegister.DAL.Models;
using System.Data.Entity;

namespace ClassRegister.DAL.Repositories
{
    public class HomeworkRepository
    {
        public void AddHomework(Homework homework)
        {
            using (var dbContext = new RegistersDbContext())
            {
                if (homework.Register != null && !dbContext.Registers.Local.Contains(homework.Register))
                {
                    dbContext.Registers.Attach(homework.Register);
                    dbContext.Entry( homework.Register).State = EntityState.Modified;
                }
                if (homework.Student != null && !dbContext.Students.Local.Contains(homework.Student))
                {
                    dbContext.Students.Attach(homework.Student);
                    dbContext.Entry(homework.Student).State = EntityState.Modified;
                }
                dbContext.Homeworks.Add(homework);
                dbContext.SaveChanges();
            }
        }
    }
}

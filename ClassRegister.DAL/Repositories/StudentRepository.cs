﻿using ClassRegister.DAL.Data;
using ClassRegister.DAL.Models;
using System.Collections.Generic;
using System.Linq;

namespace ClassRegister.DAL.Repositories
{
    public class StudentRepository
    {
        public void AddStudent(Student student)
        {
            using (var dbContext = new RegistersDbContext())
            {
                dbContext.Students.Add(student);
                dbContext.SaveChanges();
            }
        }

        public Student GetStudent(int id)
        {
            using (var dbContext = new RegistersDbContext())
            {
                return dbContext.Students
                    .SingleOrDefault(x => x.Id == id);
            }
        }

        public List<Student> GetAll()
        {
            using (var dbContext = new RegistersDbContext())
            {
                return dbContext.Students
                    .ToList();
            }
        }
    }
}

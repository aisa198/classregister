﻿using ClassRegister.DAL.Data;
using ClassRegister.DAL.Models;
using System.Data.Entity;

namespace ClassRegister.DAL.Repositories
{
    public class PresenceRepository
    {
        public void AddPresence(Presence presence)
        {
            using (var dbContext = new RegistersDbContext())
            {
                if (presence.Register != null && !dbContext.Registers.Local.Contains(presence.Register))
                {
                    dbContext.Registers.Attach(presence.Register);
                    dbContext.Entry(presence.Register).State = EntityState.Modified;
                }
                if (presence.Student != null && !dbContext.Students.Local.Contains(presence.Student))
                {
                    dbContext.Students.Attach(presence.Student);
                    dbContext.Entry(presence.Student).State = EntityState.Modified;
                }
                dbContext.Presences.Add(presence);
                dbContext.SaveChanges();
            }
        }
    }
}
